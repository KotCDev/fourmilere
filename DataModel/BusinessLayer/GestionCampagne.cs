﻿using DataModel.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DataAccessLayer;

namespace DataModel.BusinessLayer
{
    public class GestionCampagne
    {
        private DataAccessLayer.DataAccessLayer Ctx;

        public GestionCampagne(DataAccessLayer.DataAccessLayer ctx)
        {
            Ctx = ctx;
        }

        public GestionCampagne()
        {
            //Ctx = new DataAccessLayer.DataAccessLayer();
            
        }

        public IEnumerable<Campagne> GetAll()
        {
            // return Ctx.Campagnes;
            return DataAccessLayer.DataAccessLayer.GetInstance().Campagnes.ToList();
        }

        public void Ajouter(Campagne campagne)
        {
            campagne.DateCreation = DateTime.Today;
            DataAccessLayer.DataAccessLayer.GetInstance().Campagnes.Add(campagne);
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        } 

        public void Editer(Campagne campagne)
        {
            DataAccessLayer.DataAccessLayer.GetInstance().Entry(campagne).State = System.Data.Entity.EntityState.Modified;
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        }

        public int GetNbCampagne()
        {
            return DataAccessLayer.DataAccessLayer.GetInstance().Campagnes.Count();
        }

        public Campagne GetCampagneById(int? id)
        {
            return DataAccessLayer.DataAccessLayer.GetInstance().Campagnes.Find(id);
        }

        public void Supprimer(Campagne campagne)
        {
            DataAccessLayer.DataAccessLayer.GetInstance().Campagnes.Remove(campagne);
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        }

        public void SetStartDate(Campagne campagne)
        {
            campagne.DateDebut = DateTime.Today;
             DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();     
        }

        public void SetFinalDate(Campagne campagne)
        {
            campagne.DateFin = DateTime.Today;
             DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();     
        }

        public void Dispose()
        {
            DataAccessLayer.DataAccessLayer.GetInstance().Dispose();
        }

        public IEnumerable<Object> GetByRequest()
        {
            
            var TabCampagne = Ctx.Campagnes;

            var request = from LigneDeTable in TabCampagne
                          orderby LigneDeTable.DateDebut
                          select new
                          {
                              DateDebut = LigneDeTable.DateDebut
                          };

            return request.ToList();
        }

      
    }
}
