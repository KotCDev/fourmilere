﻿using DataModel.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DataAccessLayer;

namespace DataModel.BusinessLayer
{
    public class GestionFourmis
    {
        private DataAccessLayer.DataAccessLayer Ctx;

        public GestionFourmis(DataAccessLayer.DataAccessLayer ctx)
        {
            Ctx = ctx;
        }

        public GestionFourmis()
        {
           // Ctx = new DataAccessLayer.DataAccessLayer();
        }

        public IEnumerable<Fourmi> GetAll()
        {
            return DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.ToList();
        }

        public void Ajouter(Fourmi fourmi)
        {
            if(fourmi.Level == 0) { 
                fourmi.Level = 1;
            }
            fourmi.Vitesse = fourmi.Level;
            fourmi.Capacite = fourmi.Level*3;
            fourmi.Etat = Fourmi.Etats.Pause;
            DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.Add(fourmi);
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        }

        public void Editer(Fourmi fourmi)
        {
            DataAccessLayer.DataAccessLayer.GetInstance().Entry(fourmi).State = System.Data.Entity.EntityState.Modified;
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        }

        public void Supprimer(Fourmi fourmi)
        {
            DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.Remove(fourmi);
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        }

        public int GetNbFourmi()
        {
            return DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.Count();
        }

        public List<Fourmi> GetNbFourmi(Fourmi.Etats etat)
        {
             return DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.Where(fourmi => fourmi.Etat == etat).ToList();
        }

        public int GetNbFourmiDispo()
        {
            return GetNbFourmi(Fourmi.Etats.Pause).Where(fourmi => fourmi.CampagneID == null).ToList().Count();
        }

        public Fourmi GetFourmiById(int? id)
        {
            return DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.Find(id);
        }

        public IEnumerable<Object> GetByRequest()
        {

            /* var request = Ctx.Fourmis.Where(f => f.Vitesse > 10 && f.Vitesse < 50).OrderBy(f => f.Vitesse).Select(f => new
             {
                 Etats = f.Etat,
                 Vitesse = f.Vitesse
             });*/
            var TabFourmi = Ctx.Fourmis;

            var request = from LigneDeTable in TabFourmi
                          where LigneDeTable.Vitesse > 10 && LigneDeTable.Vitesse < 50
                          orderby LigneDeTable.Vitesse
                          select new
                          {
                              Etats = LigneDeTable.Etat,
                              Vitesse = LigneDeTable.Vitesse
                          };

            return request.ToList();
        }

        public void AffecterFourmi(Campagne campagne)
        {
            campagne.ListeFourmi = new List<Fourmi>();
            List<Fourmi> fourmis = GetNbFourmi(Fourmi.Etats.Pause).Where(fourmi => fourmi.CampagneID == null).ToList();
            for (int i = 0; i < campagne.NbFourmi; i++)
            {
                fourmis[i].CampagneID = campagne.CampagneID;
                fourmis[i].Campagne = campagne;
                campagne.ListeFourmi.Add(fourmis[i]);
            }
            /*foreach (Fourmi fourmi in campagne.ListeFourmi)
            {
                fourmi.CampagneID = campagne.CampagneID;
                fourmi.Campagne = campagne;
            }*/
        }

        public void LevelUp(List<Fourmi> Fourmis)
        {
            foreach(Fourmi fourmi in Fourmis)
            {
                if (fourmi.Level < 20)
                {
                    fourmi.Level++;
                    fourmi.Vitesse++;
                    fourmi.Capacite += 3;
                } else
                {
                    fourmi.Etat = Fourmi.Etats.Mort;
                }
            }
        }

        public void LevelUp(Fourmi fourmi)
        {
            if (fourmi.Level < 20)
            {
                fourmi.Level++;
                fourmi.Vitesse++;
                fourmi.Capacite += 3;
            }
            else
            {
                fourmi.Etat = Fourmi.Etats.Mort;
            }
            DataAccessLayer.DataAccessLayer.GetInstance().SaveChanges();
        }

        public IEnumerable<Fourmi.Etats> getListEtatFourmi()
        {
            return DataAccessLayer.DataAccessLayer.GetInstance().Fourmis.Select(f => f.Etat);
        }

    }
}
