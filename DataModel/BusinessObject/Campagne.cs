﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BusinessObject
{
    public class Campagne
    {
        [Key]
        public int CampagneID { get; set; }

        public DateTime? DateCreation { get; set; }

        public DateTime? DateDebut {get; set;}

        public DateTime? DateFin {get; set; }

        public String Nom { get; set; }

        public int NbFourmi { get; set; }

        public int Nourriture { get; set; }

        public virtual ICollection<Fourmi> ListeFourmi { get; set; }
    }
}
