﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.BusinessObject
{
    public class Fourmi
    {
        public enum Etats { Pause, Mort, Chasse, Retour }

        [Key]
        public int FourmiId { get; set; }

        public Etats Etat { get; set; }

        public int Vitesse { get; set; }

        public int Capacite { get; set; }

        public int Level { get; set; }

        public int? CampagneID { get; set; }

        public virtual Campagne Campagne { get; set; }
    }
}
