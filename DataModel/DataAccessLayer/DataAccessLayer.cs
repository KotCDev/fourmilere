﻿namespace DataModel.DataAccessLayer
{
    using DataModel.BusinessObject;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DataAccessLayer : DbContext
    {
        // Votre contexte a été configuré pour utiliser une chaîne de connexion « DataAccessLayer » du fichier 
        // de configuration de votre application (App.config ou Web.config). Par défaut, cette chaîne de connexion cible 
        // la base de données « DataModel.DataAccessLayer.DataAccessLayer » sur votre instance LocalDb. 
        // 
        // Pour cibler une autre base de données et/ou un autre fournisseur de base de données, modifiez 
        // la chaîne de connexion « DataAccessLayer » dans le fichier de configuration de l'application.
        public DataAccessLayer()
            : base("name=DataAccessLayer")
        {
            Database.SetInitializer<DataAccessLayer>(new CreateDatabaseIfNotExists<DataAccessLayer>());
        }

        // Ajoutez un DbSet pour chaque type d'entité à inclure dans votre modèle. Pour plus d'informations 
        // sur la configuration et l'utilisation du modèle Code First, consultez http://go.microsoft.com/fwlink/?LinkId=390109.

        private static DataAccessLayer Dal {get; set;}



        public static DataAccessLayer GetInstance()
        {
            if (Dal == null)
            {
                Dal = new DataAccessLayer();
            }
            return Dal;
        }

        public virtual DbSet<Fourmi> Fourmis { get; set; }

        public virtual DbSet<Campagne> Campagnes { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}