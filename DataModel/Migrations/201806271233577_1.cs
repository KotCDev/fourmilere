namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fourmis",
                c => new
                    {
                        FourmiId = c.Int(nullable: false, identity: true),
                        Etat = c.String(),
                        Vitesse = c.Int(nullable: false),
                        Capacite = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FourmiId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Fourmis");
        }
    }
}
