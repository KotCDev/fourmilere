namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campagnes",
                c => new
                    {
                        CampagneID = c.Int(nullable: false, identity: true),
                        DateDebut = c.DateTime(nullable: false),
                        DateFin = c.DateTime(nullable: false),
                        nom = c.String(),
                    })
                .PrimaryKey(t => t.CampagneID);
            
            AddColumn("dbo.Fourmis", "CampagneID", c => c.Int());
            CreateIndex("dbo.Fourmis", "CampagneID");
            AddForeignKey("dbo.Fourmis", "CampagneID", "dbo.Campagnes", "CampagneID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Fourmis", "CampagneID", "dbo.Campagnes");
            DropIndex("dbo.Fourmis", new[] { "CampagneID" });
            DropColumn("dbo.Fourmis", "CampagneID");
            DropTable("dbo.Campagnes");
        }
    }
}
