namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Campagnes", "DateDebut", c => c.DateTime());
            AlterColumn("dbo.Campagnes", "DateFin", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Campagnes", "DateFin", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Campagnes", "DateDebut", c => c.DateTime(nullable: false));
        }
    }
}
