namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campagnes", "NbFourmi", c => c.Int(nullable: false));
            AlterColumn("dbo.Fourmis", "Etat", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Fourmis", "Etat", c => c.String());
            DropColumn("dbo.Campagnes", "NbFourmi");
        }
    }
}
