namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campagnes", "DateCreation", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campagnes", "DateCreation");
        }
    }
}
