﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiPass
{
    class Program
    {
        public class Param
        {
            public string nom { get; set; }
            public int limite { get; set; }
        }

        static void Main(string[] args)
        {
            System.Console.WriteLine("***********App Multi Thread************");

            Param G1 = new Param();
            G1.nom = "GC1";
            G1.limite = 10;

            Param G2 = new Param();
            G2.nom = "GC2";
            G2.limite = 1000000000;

            Stopwatch Sw = new Stopwatch();
            Thread GC1 = new Thread(GrosCalcul1);
            Thread GC2 = new Thread(GrosCalcul1);
            Sw.Start();

            GC1.Start();         

            Sw.Stop();

            System.Console.WriteLine("Temps d'executon** : " + Sw.Elapsed);

            //System.Console.ReadKey();
        }

        static async void GrosCalcul1()
        {
            System.Console.WriteLine("***********Debut************");
            //Thread.Sleep(2000);

            await Task.Delay(2000);
            System.Console.WriteLine("***********Fin************");
        }

    }
}
