﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThread
{
    class Program
    {

        static void Main(string[] args)
        {
            System.Console.WriteLine("***********App Multi Thread************\n");

            Stopwatch Sw = new Stopwatch();
            Thread GC1 = new Thread(GrosCalcul1);
           
            Sw.Start();

            GC1.Start();

            Sw.Stop();

            System.Console.WriteLine("Temps d'executon : " + Sw.Elapsed);
            System.Console.WriteLine("Requête 2");
            System.Console.WriteLine("Requête 3");

            System.Console.ReadKey();
        }

        static async void GrosCalcul1()
        {
            System.Console.WriteLine("***********Debut************");
            //Thread.Sleep(2000);

            bool isFinished = false;

            isFinished = await Wok();

            if (isFinished)
            {
                System.Console.WriteLine("************Fin*************");
            }

            //await Task.Delay(2000);
            
        }

        static async Task<bool> Wok()
        {
            //Task.Delay(2000).Wait();
            Thread.Sleep(2000);
            return true;
        }
    

    }
}
