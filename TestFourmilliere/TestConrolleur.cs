﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebFourmiliere.Controllers;
using System.Web.Mvc;
using DataModel.BusinessObject;

namespace TestFourmilliere
{
    /// <summary>
    /// Description résumée pour TestConrolleur
    /// </summary>
    [TestClass]
    public class TestConrolleur
    {
        public TestConrolleur()
        {
            //
            // TODO: ajoutez ici la logique du constructeur
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active, ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestCtrlCampagne()
        {
            var controleur = new WebFourmiliere.Controllers.CampagnesController();

            var result = controleur.Details(0);

            Assert.IsTrue(result is ViewResult);

            Assert.IsTrue(((ViewResult)result).Model is Campagne);

           // Assert.AreEqual(1, NoteDeFrais)(ViewResult) resultat).Model).CampagneID);
        }
    }
}
