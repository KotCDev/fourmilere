﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataModel.DataAccessLayer;
using DataModel.BusinessLayer;
using System.Diagnostics;
using DataModel.BusinessObject;

namespace TestFourmilliere
{
    /// <summary>
    /// Description résumée pour DataModel
    /// </summary>
    [TestClass]
    public class TestDAL
    {
        GestionFourmis GF = new GestionFourmis();
        public TestDAL()
        {
            
            //
            // TODO: ajoutez ici la logique du constructeur
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active, ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestTypeDal()
        {
            Assert.IsTrue(DataAccessLayer.GetInstance() is DataAccessLayer);
        }

        [TestMethod]
        public void TestConnexion()
        {
            Assert.AreEqual(DataAccessLayer.GetInstance().Database.Exists(), true);
        }

        [TestMethod]
        public void TestListeEtatFourmi()
        {  
            Assert.IsTrue(GF.getListEtatFourmi() is IEnumerable<String>);
        }

        [TestMethod]
        public void TestListeEtatFourmi2()
        {
            Stopwatch Sw = new Stopwatch();
            IEnumerable<Fourmi.Etats> liste = new List<Fourmi.Etats>();
            Sw.Start();
            liste = GF.getListEtatFourmi();
            Sw.Stop();
            Assert.IsTrue(Sw.ElapsedMilliseconds < 500);
        }
    }
}
