﻿using DataModel.BusinessLayer;
using DataModel.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.DTO;

namespace WebAPI.Controllers
{
    public class FourmisController : ApiController
    {
        public GestionFourmis GF { get; set; }

        FourmisController()
        {
            GF = new GestionFourmis();
        }

        public IEnumerable<DTOFourmis> GET()
        {
            /* List<DTOFourmis> TempList = new List<DTOFourmis>();
             foreach(Fourmi item in GF.GetAll())
             {
                 DTOFourmis DTOF = new DTOFourmis(item);
                //TempList.Add(DTOF);
                yield return DTOF;

             }*/

              return GF.GetAll().Select(f => new DTOFourmis(f));
        }
    }
}
