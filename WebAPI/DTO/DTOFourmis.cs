﻿using DataModel.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebAPI.DTO
{
    [DataContract]
    public class DTOFourmis
    {
        private Fourmi _Fourmi { get; set; }

        public DTOFourmis(Fourmi fourmi)
        {
            this._Fourmi = fourmi;
        }

        [DataMember]
        public int FourmiId
        {
            get
            {
                return this._Fourmi.FourmiId;
            }

            set
            {
                this._Fourmi.FourmiId = value;
            }
        }

        [DataMember]
        public Fourmi.Etats Etat
        {
            get
            {
                return this._Fourmi.Etat;
            }

            set
            {
                this._Fourmi.Etat = value;
            }
        }

        [DataMember]
        public int Vitesse
        {
            get
            {
                return this._Fourmi.Vitesse;
            }

            set
            {
                this._Fourmi.Vitesse = value;
            }
        }
        [DataMember]
        public int Capacite
        {
            get
            {
                return this._Fourmi.Capacite;
            }

            set
            {
                this._Fourmi.Capacite = value;
            }
        }
        [DataMember]
        public int Level
        {
            get
            {
                return this._Fourmi.Level;
            }

            set
            {
                this._Fourmi.Level = value;
            }
        }
    }
}