﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataModel.BusinessLayer;
using DataModel.BusinessObject;
using DataModel.DataAccessLayer;

namespace WebFourmiliere.Controllers
{
    public class CampagnesController : Controller
    {
      //  private static DataAccessLayer ctx = new DataAccessLayer();

        private GestionFourmis gestionFourmis = new GestionFourmis();
       
        private GestionCampagne gestionCampagne = new GestionCampagne();

        // GET: Campagnes
        public ActionResult Index()
        {
            return View(gestionCampagne.GetAll());
        }

        // GET: Campagnes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campagne campagne = gestionCampagne.GetCampagneById(id);
            // Campagne campagne = db.Campagnes.Find(id);
            if (campagne == null)
            {
                return HttpNotFound();
            }
            return View(campagne);
        }

        // GET: Campagnes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Campagnes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CampagneID,DateDebut,DateFin,Nom,NbFourmi,Nourriture")] Campagne campagne)
        {
            if (ModelState.IsValid)
            {
                int nbFourmis = gestionFourmis.GetNbFourmiDispo();
                if (nbFourmis <= campagne.NbFourmi)
                {
                    nbFourmis = campagne.NbFourmi - nbFourmis;
                    for (int nbFourmi = 0; nbFourmi < nbFourmis; nbFourmi++)
                    {
                        Fourmi fourmi = new Fourmi();
                        gestionFourmis.Ajouter(fourmi);
                        //campagne.ListeFourmi.Add(fourmi);
                    }
                }
                gestionFourmis.AffecterFourmi(campagne);
                //db.Campagnes.Add(campagne);
                //db.SaveChanges();
                gestionCampagne.Ajouter(campagne);

                return RedirectToAction("Index");
            }

            return View(campagne);
        }


       

        // GET: Campagnes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campagne campagne = gestionCampagne.GetCampagneById(id);
           // Campagne campagne = db.Campagnes.Find(id);
            if (campagne == null)
            {
                return HttpNotFound();
            }
            return View();
        }

        // POST: Campagnes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CampagneID,DateDebut,DateFin,Nom")] Campagne campagne)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(campagne).State = EntityState.Modified;
                //db.SaveChanges();
                gestionCampagne.Editer(campagne);
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: Campagnes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
             Campagne campagne = gestionCampagne.GetCampagneById(id);
             if (campagne == null)
             {
                 return HttpNotFound();
             }
            return View();
        }

        // POST: Campagnes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Campagne campagne = gestionCampagne.GetCampagneById(id);
            foreach(Fourmi fourmi in campagne.ListeFourmi)
            {
                fourmi.CampagneID = null;
            }
            gestionCampagne.Supprimer(campagne);
           
            return RedirectToAction("Index");
        }

        /*protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               // gestionCampagne.Dispose();
               // db.Dispose();
            }
            base.Dispose(disposing);
        }*/
    }
}
