﻿using DataModel.BusinessLayer;
using DataModel.BusinessObject;
using DataModel.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebFourmiliere.Controllers
{
    public class FourmisController : Controller
    {
        private GestionFourmis GF = new GestionFourmis();

       // private DataAccessLayer db = new DataAccessLayer();

        // GET: Fourmis
        public ActionResult Index()
        {
            return View();
        }

        // GET: Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Fourmi fourmi)
        {
            GF.Ajouter(fourmi);

            return RedirectToAction("Index");
        }

        public ActionResult ListeFourmiByApi()
        {
            return View();
        }

        public ActionResult ListeFourmiByDataModel()
        {
            return PartialView("_ListeFourmiByDataModel", GF.GetAll());
        }

        // GET: Campagnes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fourmi fourmi = GF.GetFourmiById(id);
            if (fourmi == null)
            {
                return HttpNotFound();
            }
            return View(fourmi);
        }

        // POST: Campagnes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CampagneID,DateDebut,DateFin,nom")] Fourmi fourmi)
        {
            if (ModelState.IsValid)
            {
                // db.Entry(fourmi).State = EntityState.Modified;
                // db.SaveChanges();
                GF.Editer(fourmi);
                return RedirectToAction("Index");
            }
            return View(fourmi);
        }

        // GET: Campagnes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Fourmi fourmi = db.Fourmis.Find(id);
            Fourmi fourmi = GF.GetFourmiById(id);

            if (fourmi == null)
            {
                return HttpNotFound();
            }
            return View(fourmi);
        }

        // POST: Campagnes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Fourmi fourmi = db.Fourmis.Find(id);
            Fourmi fourmi = GF.GetFourmiById(id);

            // db.Fourmis.Remove(fourmi);
            // db.SaveChanges();
            GF.Supprimer(fourmi);
            return RedirectToAction("Index");
        }

      /*  protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
         //       db.Dispose();
            }
            base.Dispose(disposing);
        }*/
    }
}