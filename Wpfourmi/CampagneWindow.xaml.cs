﻿using DataModel.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Wpfourmi.Tools;

namespace Wpfourmi
{
    /// <summary>
    /// Logique d'interaction pour CampagneWindow.xaml
    /// </summary>
    public partial class CampagneWindow : Window
    {
        public Campagne campagne;

        public int SelectedTarget = 0;

        public bool CampagneFinish = false;

        public List<Image> ListImageFourmi = new List<Image>();

        System.Windows.Threading.DispatcherTimer dispatcherTimer;
        int idep;

        public CampagneWindow(Campagne campagne)
        {
            InitializeComponent();
            this.campagne = campagne;

            Data.GetDataInstanceGC().SetStartDate(campagne);

            FourmilereProgressBar.Maximum = campagne.Nourriture;
            FourmilereStackValue.Content = 0;
            NourritureProgressBar.Maximum = campagne.Nourriture;
            NourritureProgressBar.Value = campagne.Nourriture;
            NourritureStackValue.Content = campagne.Nourriture;
            //CreateGrid();
            InitializeFourmi();
        }

       
        public void CreateGrid()
        {
            for (int i = 0; i < 100; i++)
            {
                Border border = new Border
                {
                    Height = 5,
                    Width = 5,
                    BorderBrush = Brushes.Black
                };
                
                Grid.SetColumn(border, i);
            }
        }

        public void InitializeFourmi()
        {
            /*Rectangle fourmiRectangle = new Rectangle();
            fourmiRectangle.Width = 50;
            fourmiRectangle.Height = 30;
            fourmiRectangle.Fill = new SolidColorBrush(System.Windows.Media.Colors.Black);
            */
            for (int i = 0; i < campagne.ListeFourmi.Count(); i++)
            {
                Image ImageFourmi = new Image
                {
                    Width = 30,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Margin = new Thickness(100, 50 + i * 50, 0, 0)
                };

                BitmapImage logo = new BitmapImage();
                logo.BeginInit();
                logo.UriSource = new Uri("E:/Cours/MSIA/2eme annee/C sharp/Fourmiliere/Wpfourmi/Images/fourmi.png");
                logo.EndInit();

                ImageFourmi.Source = logo;

                ListImageFourmi.Add(ImageFourmi);

                Grid.Children.Add(ImageFourmi);

            }

            
        }

        private void BT_Demarrage(object sender, RoutedEventArgs e)
        {

            BtDemarrage.Visibility = Visibility.Hidden;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
            dispatcherTimer.Start();


        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {

           

            foreach (Image image in ListImageFourmi)
            {
                SelectedTarget = ListImageFourmi.IndexOf(image);
                // MoveTo(image, 10 * campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(image)).Vitesse, 0);
                MoveTo(image, Grid.ActualWidth - 100, (Grid.ActualWidth/32) - campagne.ListeFourmi.ElementAt(SelectedTarget).Vitesse, 0);

                campagne.ListeFourmi.ElementAt(SelectedTarget).Etat = Fourmi.Etats.Chasse;
               // campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(image)).Vitesse;
            }
            //this.Visibility = System.Windows.Visibility.Visible;
            (sender as DispatcherTimer).Stop();
        }

        private void AnimeCompleted(Image target, object sender, EventArgs e)
        {

            if (campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Etat == Fourmi.Etats.Chasse)
            {
                campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Etat = Fourmi.Etats.Retour;
                NourritureProgressBar.Value -= campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Capacite;
                int Value = (int)(NourritureStackValue.Content) - campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Capacite;
                if (Value <= 0)
                {
                    Value = 0;
                    CampagneFinish = true;
                }
                NourritureStackValue.Content = Value;
                NourritureProgressPourcentage.Content =  (int) (NourritureProgressBar.Value * 100 / NourritureProgressBar.Maximum) + "%";
                //if (!CampagneFinish)
                //{
                    MoveTo(target, 100, (Grid.ActualWidth / 32) - campagne.ListeFourmi.ElementAt(SelectedTarget).Vitesse, Grid.ActualWidth - 200);
                //}
            }
            else
            {
                campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Etat = Fourmi.Etats.Chasse;
                FourmilereProgressBar.Value += campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Capacite;
                int Value = (int)(FourmilereStackValue.Content) + campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Capacite;
                if (Value >= campagne.Nourriture)
                {
                    Value = campagne.Nourriture;
                }
                FourmilereStackValue.Content = Value;
                FourmiliereProgressPourcentage.Content = (int)(FourmilereProgressBar.Value * 100 / FourmilereProgressBar.Maximum) + "%";
                Data.GetDataInstanceGF().LevelUp(campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)));
                if (!CampagneFinish || campagne.ListeFourmi.ElementAt(ListImageFourmi.IndexOf(target)).Etat == Fourmi.Etats.Mort)
                {   
                    MoveTo(target, Grid.ActualWidth - 100, (Grid.ActualWidth / 32) - campagne.ListeFourmi.ElementAt(SelectedTarget).Vitesse, 0);
                } else if(CampagneFinish && ListImageFourmi.IndexOf(target) == ListImageFourmi.Count-1)
                {
                    Data.GetDataInstanceGC().SetFinalDate(campagne);
                    FinishLabel.Visibility = Visibility.Visible;
                }
            }
        }



        public void MoveTo(Image target, double newX, double Vitesse, double test)
        {
            Vector offset = VisualTreeHelper.GetOffset(target);
            var top = offset.Y;
            var left = offset.X;
            TranslateTransform trans = new TranslateTransform();
            target.RenderTransform = trans;
            
            DoubleAnimation animAller = new DoubleAnimation(test, newX - left, TimeSpan.FromSeconds(Vitesse));

            animAller.Completed += (sender, eventArgs) =>
            {
                AnimeCompleted(target, sender, eventArgs);
            };

         
            trans.BeginAnimation(TranslateTransform.XProperty, animAller);
    
        }



        private void BT_Retour(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

    }
}
