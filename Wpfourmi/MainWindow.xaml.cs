﻿using DataModel.BusinessLayer;
using DataModel.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Wpfourmi.Tools;

namespace Wpfourmi
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            LabelCampagne.Visibility = Visibility.Hidden;
            LabelNbFourmi.Visibility = Visibility.Hidden;
            DetailCampagne.Visibility = Visibility.Hidden;
            ListeFourmi.Visibility = Visibility.Hidden;
            BTLancerCampagne.Visibility = Visibility.Hidden;

            InitCampagne();
        }

        private void InitCampagne()
        {
            List<Campagne> campagnes = Data.GetDataInstanceGC().GetAll().ToList();
            foreach (Campagne campagne in campagnes)
            {
                ListCampagne.Items.Add(campagne);
            }
       
        }

        private void BT_Lancer_Campagne(object sender, RoutedEventArgs e)
        {
            CampagneWindow campagneWindow = new CampagneWindow(ListCampagne.SelectedItem as Campagne);
            campagneWindow.Show();
            this.Close();
        }

        private void ListCampagne_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Campagne campagne = (ListCampagne.SelectedItem as Campagne);

            ICollection<Fourmi> listeFourmi = campagne.ListeFourmi;

            LabelCampagne.Content = campagne.Nom;


            //DetailCampagne.Items.Add(campagne);
            List<Campagne> campagnes = new List<Campagne>
            {
                campagne
            };
            DetailCampagne.ItemsSource = campagnes;

            DetailCampagne.Columns.RemoveAt(7);
            DetailCampagne.Columns.RemoveAt(4);        
            DetailCampagne.Columns.RemoveAt(0);

            LabelNbFourmi.Content = "Nombre de fourmis : " + listeFourmi.Count();

            ListeFourmi.ItemsSource = listeFourmi.ToList();
            ListeFourmi.Columns.RemoveAt(6);
            ListeFourmi.Columns.RemoveAt(5);
            ListeFourmi.Columns.RemoveAt(0);   

            LabelCampagne.Visibility = Visibility.Visible;
            LabelNbFourmi.Visibility = Visibility.Visible;
            DetailCampagne.Visibility = Visibility.Visible;
            ListeFourmi.Visibility = Visibility.Visible;
            if (campagne.DateFin != null)
            {
                BTLancerCampagne.Visibility = Visibility.Hidden;
            }
            else
            {
                BTLancerCampagne.Visibility = Visibility.Visible;     
            }
        }
    }
}
