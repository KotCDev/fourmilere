﻿using DataModel.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpfourmi.Tools
{
    public class Data
    {
        private static GestionFourmis GF;
        private static GestionCampagne GC;

        private Data()
        {
            GF = new GestionFourmis();
            GC = new GestionCampagne();
        }

        public static GestionFourmis GetDataInstanceGF()
        {
            if(GF == null)
            {
                GF = new GestionFourmis();
            }

            return GF;
        }

        public static GestionCampagne GetDataInstanceGC()
        {
            if (GC == null)
            {
                GC = new GestionCampagne();
            }

            return GC;
        }

    }
}
